const admin = require('firebase-admin');
const { db } = require('./firebase');

function Send(req,res){
    res.send(req.params.id);
    db.collection('devices').doc(req.params.id).get().then(doc => {
        var registrationTokens = doc._fieldsProto.token.stringValue;
        var payload = {
            data: {
                title: 'แจ้งเตือน',
                body: 'มีคำขอที่คุณต้องอนุมัติ เลขที่ '+req.params.number
            }
        };
    
        admin.messaging().sendToDevice(registrationTokens, payload)
            .then(function (response) {
                console.log('Successfully sent message:', response);
            })
            .catch(function (error) {
                console.log('Error sending message:', error);
            });
    }).catch(err=>{
        console.log('Have no id :'+req.params.id);
    })
}

module.exports = {Send};
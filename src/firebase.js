const admin = require('firebase-admin');
const ServiceAccount = require('./../ServiceAccountKey.json');

admin.initializeApp({
    credential: admin.credential.cert(ServiceAccount),
    databaseURL: 'https://ionic-pr-po-app.firebaseio.com'
});

const db = admin.firestore();
db.settings({
    timestampsInSnapshots: true
});


module.exports = {db};